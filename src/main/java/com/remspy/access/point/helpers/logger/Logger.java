package com.remspy.access.point.helpers.logger;

import com.remspy.access.point.helpers.logger.enums.LOG_LEVEL;

import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {

    private static boolean sDebugOn;

    private static LOG_LEVEL sLogLevel;

    private static DateTimeFormatter sDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public Logger(boolean debugOn, LOG_LEVEL logLevel) {
        Logger.sDebugOn = debugOn;
        Logger.sLogLevel = logLevel;
    }

    public static void d(Object msg, Object ...args) {
        log(System.out, null, msg, args);
    }

    public static void e(Object msg, Object ...args) {
        log(System.err, "ERROR", msg, args);
    }

    public static void w(Object msg, Object ...args) {
        log(System.err, "WARNING", msg, args);
    }

    public static void log(PrintStream stream, String logType, Object msg, Object ...args) {
        if (!sDebugOn) {
            return;
        }

        Thread currentThread = Thread.currentThread();

        StackTraceElement[] stack = currentThread.getStackTrace();
        StackTraceElement lastOfStack = stack[3];

        String className = lastOfStack.getClassName();
        String fileName = lastOfStack.getFileName();
        String methodName = lastOfStack.getMethodName();

        StringBuilder builder = new StringBuilder();

        if (logType != null) {
            builder.append(logType).append(" ");
        }

        String date = LocalDateTime.now().format(sDateTimeFormatter);
        builder.append("[").append(date).append("] ");

        String message = String.format(String.valueOf(msg), args);
        String log = "";

        switch (sLogLevel) {
            case LOW:
                log = String.format("%s(): %s",  methodName, message);
                break;

            case MEDIUM:
                log = String.format("%s -> %s(): %s", className, methodName, message);
                break;

            case HIGH:
                log = String.format("%s[%s, Thread: %s #%d] -> %s(): %s", fileName, className, currentThread.getName(), currentThread.getId(), methodName, message);
                break;
        }

        builder.append(log);

        stream.println(builder.toString());
    }

}
