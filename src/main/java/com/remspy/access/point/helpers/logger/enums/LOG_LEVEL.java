package com.remspy.access.point.helpers.logger.enums;

public enum LOG_LEVEL {
    LOW,
    MEDIUM,
    HIGH
}
