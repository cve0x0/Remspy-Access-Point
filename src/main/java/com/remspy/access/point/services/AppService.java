package com.remspy.access.point.services;

import com.remspy.access.point.common.DependencyInjection;
import com.remspy.access.point.helpers.logger.Logger;
import com.remspy.access.point.helpers.logger.enums.LOG_LEVEL;
import com.remspy.access.point.interfaces.*;
import com.remspy.access.point.modules.consumer.LoadBalanceModule;
import com.remspy.access.point.modules.database.DatabaseModule;
import com.remspy.access.point.modules.listener.ListenerModule;
import com.remspy.access.point.modules.statements.PreparedStatementsModule;

import java.util.ArrayList;

public class AppService {

    private ArrayList<Module> mModules = new ArrayList<>(), mRunningModules = new ArrayList<>();

    private static AppService sAppServiceInstance;

    private void addModules() {
//        mModules.add(new DatabaseModule());
//        mModules.add(new PreparedStatementsModule());
        mModules.add(new ListenerModule());
        mModules.add(new LoadBalanceModule(mModules, 1));
    }

    private void initDependencyInjection() {
        ArrayList<Object> modules = new ArrayList<>();

        modules.add(this);
        modules.addAll(mModules);

        DependencyInjection.inject(modules);
    }

    @SuppressWarnings("unchecked")
    public static <T> T accessStaticModule(Class<T> moduleClass) {
        for (Module module: sAppServiceInstance.mModules) {
            if (module instanceof ModuleStatic && module.getClass() == moduleClass) {
                return (T) module;
            }
        }

        return null;
    }

    private void initModules() {
        for (Module module: mModules) {
            if (module instanceof ModuleInit) {
                ((ModuleInit) module).onInit();
            }

            if (module instanceof ModuleStart) {
                ((ModuleStart) module).onStart();
            }

            if (module instanceof ModuleRunnable) {
                new Thread((ModuleRunnable) module).start();
            }

            mRunningModules.add(module);
        }
    }

    public ArrayList<Module> getRunningModules() {
        return mRunningModules;
    }

    public void run() {
        sAppServiceInstance = this;

        new Logger(true, LOG_LEVEL.MEDIUM);

        addModules();
        initDependencyInjection();
        initModules();
    }

}
