package com.remspy.access.point.modules.providers.locations;

import com.remspy.access.point.annotations.Inject;
import com.remspy.access.point.common.ClientPackage;
import com.remspy.access.point.helpers.logger.Logger;
import com.remspy.access.point.interfaces.Module;
import com.remspy.access.point.interfaces.ModulePackage;
import com.remspy.access.point.modules.providers.locations.models.LocationModel;
import com.remspy.access.point.modules.statements.PreparedStatementsModule;
import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.TransferList;

public class LocationsProvider implements Module, ModulePackage {

    @Inject
    private PreparedStatementsModule mPreparedStatementsModule;

    @Override
    public void onPackageProcessing(ClientPackage clientPackage) throws Exception {
        BufferStream buffer = clientPackage.getBufferStream();
        TransferList<LocationModel> locations = buffer.getTransferList(LocationModel.class);

        for (LocationModel location: locations) {
//            Logger.d("Location: " + location.getLatitude() + " " + location.getLongitude());
//            mPreparedStatementsModule.execute(1, location);
        }
    }

}
