package com.remspy.access.point.modules.providers.events;

import com.remspy.access.point.annotations.Inject;
import com.remspy.access.point.common.ClientPackage;
import com.remspy.access.point.helpers.logger.Logger;
import com.remspy.access.point.interfaces.Module;
import com.remspy.access.point.interfaces.ModulePackage;
import com.remspy.access.point.modules.providers.events.models.EventModel;
import com.remspy.access.point.modules.statements.PreparedStatementsModule;
import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.TransferList;

public class EventsProvider implements Module, ModulePackage {

    @Inject
    private PreparedStatementsModule mPreparedStatementsModule;

    @Override
    public void onPackageProcessing(ClientPackage clientPackage) throws Exception {
        BufferStream buffer = clientPackage.getBufferStream();
        TransferList<EventModel> events = buffer.getTransferList(EventModel.class);

        for (EventModel eventModel: events) {
//            Logger.d("Event: " + eventModel.getStartDate());
//            mPreparedStatementsModule.execute(1, eventModel);
        }
    }

}
