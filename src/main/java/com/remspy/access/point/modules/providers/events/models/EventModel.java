package com.remspy.access.point.modules.providers.events.models;

import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.interfaces.DeserializePackage;

public class EventModel implements DeserializePackage {

    private int id;

    private long startDate;

    private long endDate;

    private String location;

    private String description;

    public int getId() {
        return id;
    }

    public long getStartDate() {
        return startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public void onDeserialize(BufferStream buffer) throws Exception {
        id = buffer.getInt();
        startDate = buffer.getLong();
        endDate = buffer.getLong();
        location = buffer.getChunkString();
        description = buffer.getChunkString();
    }

}
