package com.remspy.access.point.modules.providers.messages;

import com.remspy.access.point.annotations.Inject;
import com.remspy.access.point.common.ClientPackage;
import com.remspy.access.point.helpers.logger.Logger;
import com.remspy.access.point.interfaces.Module;
import com.remspy.access.point.interfaces.ModulePackage;
import com.remspy.access.point.modules.providers.messages.models.MessageModel;
import com.remspy.access.point.modules.statements.PreparedStatementsModule;
import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.TransferList;

public class MessagesProvider implements Module, ModulePackage {

    @Inject
    private PreparedStatementsModule mPreparedStatementsModule;

    @Override
    public void onPackageProcessing(ClientPackage clientPackage) throws Exception {
        BufferStream buffer = clientPackage.getBufferStream();
        TransferList<MessageModel> messages = buffer.getTransferList(MessageModel.class);

        for (MessageModel messageModel: messages) {
//            Logger.d("Message: " + messageModel.getAddress());
//            mPreparedStatementsModule.execute(1, messageModel);
        }
    }

}
