package com.remspy.access.point.modules.providers.contacts.models;

import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.interfaces.DeserializePackage;

public class ContactGroupModel implements DeserializePackage {

    private String data;

    private String type;

    public String getData() {
        return data;
    }

    public String getType() {
        return type;
    }

    @Override
    public void onDeserialize(BufferStream buffer) throws Exception {
        data = buffer.getChunkString();
        type = buffer.getChunkString();
    }

}