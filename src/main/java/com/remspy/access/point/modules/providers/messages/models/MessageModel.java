package com.remspy.access.point.modules.providers.messages.models;

import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.interfaces.DeserializePackage;

public class MessageModel implements DeserializePackage {

    private int id;

    private String address;

    private String body;

    private byte type;

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getBody() {
        return body;
    }

    public byte getType() {
        return type;
    }

    @Override
    public void onDeserialize(BufferStream buffer) throws Exception {
        id = buffer.getInt();
        address = buffer.getChunkString();
        body = buffer.getChunkString();
        type = buffer.get();
    }

}
