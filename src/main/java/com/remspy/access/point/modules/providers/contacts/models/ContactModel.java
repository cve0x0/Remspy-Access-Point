package com.remspy.access.point.modules.providers.contacts.models;

import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.interfaces.DeserializePackage;
import com.remspy.access.point.net.TransferList;

public class ContactModel implements DeserializePackage {

    private int id;

    private String name;

    private String note;

    private TransferList<ContactGroupModel> contactAddresses;

    private TransferList<ContactGroupModel> contactPhones;

    private TransferList<ContactGroupModel> contactEmails;

    private TransferList<ContactGroupModel> contactIMs;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNote() {
        return note;
    }

    public TransferList<ContactGroupModel> getContactAddresses() {
        return contactAddresses;
    }

    public TransferList<ContactGroupModel> getContactPhones() {
        return contactPhones;
    }

    public TransferList<ContactGroupModel> getContactEmails() {
        return contactEmails;
    }

    public TransferList<ContactGroupModel> getContactIMs() {
        return contactIMs;
    }

    @Override
    public void onDeserialize(BufferStream buffer) throws Exception {
        id = buffer.getInt();
        name = buffer.getChunkString();
        note = buffer.getChunkString();

        contactAddresses = buffer.getTransferList(ContactGroupModel.class);
        contactPhones = buffer.getTransferList(ContactGroupModel.class);
        contactEmails = buffer.getTransferList(ContactGroupModel.class);
        contactIMs = buffer.getTransferList(ContactGroupModel.class);
    }

}