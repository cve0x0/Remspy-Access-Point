package com.remspy.access.point.modules.consumer;

import com.remspy.access.point.annotations.Injectable;
import com.remspy.access.point.interfaces.Module;
import com.remspy.access.point.interfaces.ModuleConsumer;
import com.remspy.access.point.interfaces.ModuleRunnable;

import java.util.ArrayList;

@Injectable
public class LoadBalanceModule implements Module {

    private ArrayList<ModuleConsumer> mConsumers = new ArrayList<>();

    public LoadBalanceModule(ArrayList<Module> modules, int consumers) {
        while (consumers-- > 0) {
            ModuleRunnable consumerModule = new ConsumerModule(modules);
            mConsumers.add((ModuleConsumer) consumerModule);
            modules.add((Module) consumerModule);
        }
    }

    public ModuleConsumer getConsumer() {
        ModuleConsumer consumer = null;
        int remaining = -1;

        for (ModuleConsumer service: mConsumers) {
            if (service.remainingCapacity() > remaining) {
                consumer = service;
                remaining = service.remainingCapacity();
            }
        }

        return consumer;
    }

}
