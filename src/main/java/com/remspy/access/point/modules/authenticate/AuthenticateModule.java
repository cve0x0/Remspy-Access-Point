package com.remspy.access.point.modules.authenticate;

import com.remspy.access.point.annotations.Injectable;
import com.remspy.access.point.common.ClientPackage;
import com.remspy.access.point.interfaces.Module;
import com.remspy.access.point.interfaces.ModulePackage;
import com.remspy.access.point.net.BufferStream;

@Injectable
public class AuthenticateModule implements Module, ModulePackage {

    @Override
    public void onPackageProcessing(ClientPackage clientPackage) throws Exception {

    }

}
