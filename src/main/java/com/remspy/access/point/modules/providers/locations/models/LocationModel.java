package com.remspy.access.point.modules.providers.locations.models;

import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.interfaces.DeserializePackage;

public class LocationModel implements DeserializePackage {

    private double latitude;

    private double longitude;

    private long timestamp;

    private float accuracy;

    private double altitude;

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public double getAltitude() {
        return altitude;
    }

    @Override
    public void onDeserialize(BufferStream buffer) throws Exception {
        latitude = buffer.getDouble();
        longitude = buffer.getDouble();
        timestamp = buffer.getLong();
        accuracy = buffer.getFloat();
        altitude = buffer.getDouble();
    }
}
