package com.remspy.access.point.modules.providers.contacts;

import com.remspy.access.point.annotations.Inject;
import com.remspy.access.point.common.ClientPackage;
import com.remspy.access.point.helpers.logger.Logger;
import com.remspy.access.point.interfaces.Module;
import com.remspy.access.point.interfaces.ModulePackage;
import com.remspy.access.point.modules.providers.contacts.models.ContactModel;
import com.remspy.access.point.modules.statements.PreparedStatementsModule;
import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.TransferList;

public class ContactsProvider implements Module, ModulePackage {

    @Inject
    private PreparedStatementsModule mPreparedStatementsModule;

    @Override
    public void onPackageProcessing(ClientPackage clientPackage) throws Exception {
        BufferStream buffer = clientPackage.getBufferStream();
        TransferList<ContactModel> contacts = buffer.getTransferList(ContactModel.class);

        for (ContactModel contactModel: contacts) {
//            Logger.d("Contact: " + contactModel.getName());
//            mPreparedStatementsModule.execute(1, contactModel);
        }
    }

}
