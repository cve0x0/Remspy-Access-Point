package com.remspy.access.point.modules.database;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.remspy.access.point.annotations.Injectable;
import com.remspy.access.point.interfaces.Module;
import com.remspy.access.point.interfaces.ModuleInit;

@Injectable
public class DatabaseModule implements Module, ModuleInit {

    private Cluster mCluster;

    private Session mSession;

    @Override
    public void onInit() {
        mCluster = Cluster.builder()
                .addContactPoints("127.0.0.1")
                .withPort(9042)
                .build();

        mSession = mCluster.connect();
        mSession.execute("USE remspy;");
    }

    public Cluster getCluster() {
        return mCluster;
    }

    public Session getSession() {
        return mSession;
    }

    public void executeAsync(BoundStatement statement) {
        mSession.executeAsync(statement);
    }

}
