package com.remspy.access.point.modules.listener;

import com.remspy.access.point.annotations.Inject;
import com.remspy.access.point.common.ClientPackage;
import com.remspy.access.point.common.PackageBufferReader;
import com.remspy.access.point.common.RequestSession;
import com.remspy.access.point.environment.Environment;
import com.remspy.access.point.helpers.logger.Logger;
import com.remspy.access.point.interfaces.Module;
import com.remspy.access.point.interfaces.ModuleRunnable;
import com.remspy.access.point.modules.consumer.LoadBalanceModule;
import com.remspy.access.point.net.BufferStream;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class ListenerModule implements Module, ModuleRunnable {

    private int mUUID = 1000000;

    private Selector mSelector;

    private volatile boolean mIsRunning = true;

    private ServerSocketChannel mServerSocketChannel;

    private ByteBuffer mBuffer = ByteBuffer.allocateDirect(2*1024*1024);

    @Inject
    private LoadBalanceModule mLoadBalanceModule;

    @Override
    public void run() {
        Logger.d("Start");

        mIsRunning = true;

        try {
            listen(Environment.PORT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listen(int port) throws IOException {
        Logger.d("Listen on port %d", port);

        mSelector = Selector.open();

        mServerSocketChannel = ServerSocketChannel.open();
        InetSocketAddress hostAddr = new InetSocketAddress("localhost", port);

        mServerSocketChannel.bind(hostAddr);
        mServerSocketChannel.configureBlocking(false);

        int ops = mServerSocketChannel.validOps();
        SelectionKey selectKy = mServerSocketChannel.register(mSelector, ops, null);

        while (isRunning()) {
            if (mSelector.select() == 0) {
                continue;
            }

            Set<SelectionKey> selectedKeys = mSelector.selectedKeys();
            Iterator<SelectionKey> keysIterator = selectedKeys.iterator();

            while (keysIterator.hasNext()) {
                SelectionKey selectionKey = keysIterator.next();
                keysIterator.remove();

                if (!selectionKey.isValid()) {
                    continue;
                }

                try {
                    if (selectionKey.isAcceptable()) {
                        onAccept();
                    } else if (selectionKey.isReadable()) {
                        if (!onRead(selectionKey)) {
                            onDisconnect(selectionKey);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    onDisconnect(selectionKey);
                }
            }
        }

        selectKy.cancel();
        mServerSocketChannel.close();
        mSelector.close();
    }

    private void onAccept() throws IOException {
        SocketChannel socketChannel = mServerSocketChannel.accept();
        int UUID = nextUUID();

        Logger.d("Accept connection [UUID: %d, Address: %s]", UUID, socketChannel.getRemoteAddress().toString());

        socketChannel.configureBlocking(false);
        socketChannel.register(mSelector, SelectionKey.OP_READ, new RequestSession(UUID, socketChannel));
    }

    private boolean onRead(SelectionKey selectionKey) throws IOException {
        RequestSession session = (RequestSession) selectionKey.attachment();
        SocketChannel socketChannel = (SocketChannel) selectionKey.channel();

        if (socketChannel.read(mBuffer) == -1) {
            return false;
        }

        mBuffer.flip();

        PackageBufferReader reader = session.getPackageReader();

        boolean success = reader.put(mBuffer, buffer -> {
            try {
                mLoadBalanceModule.getConsumer().dispatch(new ClientPackage(session, reader.getPackageId(), new BufferStream(buffer)));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        mBuffer.clear();

        return success;
    }

    private void onDisconnect(SelectionKey selectionKey) {
        try {
            RequestSession session = (RequestSession) selectionKey.attachment();
            int UUID = 0;
            if (session != null) {
                UUID = session.getUUID();
                session.destroy();
            }

            SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
            Logger.d("Disconnect [UUID: %d, Address: %s]", UUID, socketChannel.getRemoteAddress().toString());

            socketChannel.close();
            selectionKey.cancel();
        } catch (Exception ignored) {

        }
    }

    private int nextUUID() {
        return mUUID++;
    }

    @Override
    public void terminate() {
        mIsRunning = false;
    }

    @Override
    public boolean isRunning() {
        return mIsRunning;
    }

}