package com.remspy.access.point.modules.providers.calls.models;

import com.remspy.access.point.net.BufferStream;
import com.remspy.access.point.net.interfaces.DeserializePackage;

public class PhoneCallModel implements DeserializePackage {

    private int id;

    private String number;

    private String name;

    private long timestamp;

    private int duration;

    private byte type;

    public int getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getDuration() {
        return duration;
    }

    public byte getType() {
        return type;
    }

    @Override
    public void onDeserialize(BufferStream buffer) throws Exception {
        id = buffer.getInt();
        number = buffer.getChunkString();
        name = buffer.getChunkString();
        timestamp = buffer.getLong();
        duration = buffer.getInt();
        type = buffer.get();
    }

}
