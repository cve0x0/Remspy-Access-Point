package com.remspy.access.point.interfaces;

public interface ModuleRunnable extends Runnable {

    void terminate();

    boolean isRunning();

}
