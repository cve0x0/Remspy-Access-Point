package com.remspy.access.point.interfaces;

import com.remspy.access.point.common.ClientPackage;

public interface ModulePackage {

    void onPackageProcessing(ClientPackage clientPackage) throws Exception;

}
