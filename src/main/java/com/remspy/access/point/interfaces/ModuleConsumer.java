package com.remspy.access.point.interfaces;

import com.remspy.access.point.common.ClientPackage;

public interface ModuleConsumer {

    void dispatch(ClientPackage clientPackage) throws InterruptedException;

    int remainingCapacity();

}
