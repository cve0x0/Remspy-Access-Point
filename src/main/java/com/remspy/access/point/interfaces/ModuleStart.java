package com.remspy.access.point.interfaces;

public interface ModuleStart {

    void onStart();

}
