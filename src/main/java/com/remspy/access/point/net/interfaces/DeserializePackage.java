package com.remspy.access.point.net.interfaces;

import com.remspy.access.point.net.BufferStream;

public interface DeserializePackage {

    void onDeserialize(BufferStream buffer) throws Exception;

}
