package com.remspy.access.point.net.enums;

public enum TransferFieldTypes {
    Bool,
    Byte,
    Short,
    Int,
    Long,
    Float,
    Double,
    String,
    ChunkString,
    List
}
