package com.remspy.access.point.net.interfaces;

import java.nio.ByteBuffer;

public interface OnBufferReceive {

    void onBufferReceive(ByteBuffer buffer);

}
