package com.remspy.access.point.net.utils;

import sun.misc.Cleaner;
import sun.nio.ch.DirectBuffer;

import java.nio.ByteBuffer;

public class BufferUtils {

    public static void free(ByteBuffer buffer) {
        if (!buffer.isDirect()) {
            return;
        }

        Cleaner cleaner = ((DirectBuffer) buffer).cleaner();
        if (cleaner == null) {
            return;
        }

        cleaner.clean();
    }

    public static ByteBuffer allocate(int size) {
        return ByteBuffer.allocateDirect(size);
    }

    public static int[] toArray(ByteBuffer buffer) {
        int remain = buffer.remaining();
        int[] arr = new int[remain];

        buffer.mark();
        for (int i = 0; i < remain; i++) {
            arr[i] = buffer.get();
        }
        buffer.reset();

        return arr;
    }

}
