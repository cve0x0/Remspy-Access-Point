package com.remspy.access.point.net.interfaces;

import com.remspy.access.point.net.BufferStream;

public interface SerializablePackage {

    void onSerialize(BufferStream buffer) throws Exception;

}
