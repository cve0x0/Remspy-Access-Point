package com.remspy.access.point.net;

import com.remspy.access.point.net.annotations.TransferField;
import com.remspy.access.point.net.interfaces.DeserializePackage;
import com.remspy.access.point.net.interfaces.SerializablePackage;
import com.remspy.access.point.net.utils.BufferUtils;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.AbstractList;

@SuppressWarnings("ALL")
public class BufferStream {

    private ByteBuffer mBuffer;

    public BufferStream(int size) {
        mBuffer = BufferUtils.allocate(size);
    }

    public BufferStream(ByteBuffer buffer) {
        mBuffer = buffer;
    }

    public byte get() {
        return mBuffer.get();
    }

    public BufferStream put(byte b) {
        mBuffer.put(b);
        return this;
    }

    public void get(byte[] buf) {
        mBuffer.get(buf);
    }

    public BufferStream put(byte[] b) {
        mBuffer.put(b);
        return this;
    }

    public BufferStream putUnsignedByte(short b) {
        put((byte) b);
        return this;
    }

    public short getUnsignedByte() {
        return (short) (get() & 0xFF);
    }

    public char getChar() {
        return mBuffer.getChar();
    }

    public BufferStream putChar(char c) {
        mBuffer.putChar(c);
        return this;
    }

    public boolean getBoolean() {
        return get() == 1;
    }

    public BufferStream setBoolean(boolean b) {
        put((byte) (b ? 1 : 0));
        return this;
    }

    public short getShort() {
        return mBuffer.getShort();
    }

    public BufferStream putShort(short i) {
        mBuffer.putShort(i);
        return this;
    }

    public int getInt() {
        return mBuffer.getInt();
    }

    public BufferStream putInt(int i) {
        mBuffer.putInt(i);
        return this;
    }

    public long getLong() {
        return mBuffer.getLong();
    }

    public BufferStream putLong(long l) {
        mBuffer.putLong(l);
        return this;
    }

    public float getFloat() {
        return mBuffer.getFloat();
    }

    public BufferStream putFloat(float v) {
        mBuffer.putFloat(v);
        return this;
    }

    public double getDouble() {
        return mBuffer.getDouble();
    }

    public BufferStream putDouble(double v) {
        mBuffer.putDouble(v);
        return this;
    }

    public String getChunkString() throws Exception {
        return getChunkString(3145728); // 3 MB
    }

    public String getChunkString(int maxSize) throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        do {
            short size = getUnsignedByte();

            maxSize -= size;
            if (maxSize < 0) {
                throw new Exception("String is too big.");
            }

            byte[] buf = new byte[size];
            get(buf);
            out.write(buf, 0, size);

            if (size < 255) {
                break;
            }
        } while (mBuffer.hasRemaining());

        return out.toString();
    }

    public BufferStream putChunkString(String s) {
        if (s == null) {
            putUnsignedByte((short) 0);
            return this;
        }

        byte[] buf = s.getBytes();
        int size = buf.length;
        int offset = 0;

        do {
            short blockSize = (short) (size > 255 ? 255 : size);
            putUnsignedByte(blockSize);
            mBuffer.put(buf, offset, blockSize);
            offset += blockSize;
            size -= blockSize;
        } while (size > 0);

        return this;
    }

    public String getString() throws Exception {
        return getString(3145728); // 3 MB
    }

    public String getString(int maxSize) throws Exception {
        int size = getInt();

        if (size == 0) {
            return new String();
        } else if (size < 0) {
            throw new Exception("Invalid string size.");
        } else if (size > maxSize) {
            throw new Exception("String is too big.");
        }

        byte[] buf = new byte[size];
        get(buf);

        return new String(buf);
    }

    public BufferStream putString(String s) {
        if (s == null) {
            putInt(0);
            return this;
        }

        putInt(s.length());
        put(s.getBytes());
        return this;
    }

    public <T> TransferList<T> getTransferList(Class<T> listGenericType) throws Exception {
        short size = getShort();

        if (size < 0) {
            throw new Exception("Invalid list size.");
        } else if (size > 1024) {
            throw new Exception("List size is too big.");
        }

        TransferList<T> list = new TransferList<>();
        list.ensureCapacity(size);

        while (size-- > 0) {
            list.add(getTransferClass(listGenericType));
        }

        return list;
    }

    public BufferStream putTransferList(AbstractList list) throws Exception {
        if (list == null) {
            putShort((short) 0);
            return this;
        }

        putShort((short) list.size());

        for (Object o: list) {
            putTransferClass(o);
        }

        return this;
    }

    public <T> T getTransferClass(Class<T> genericType) throws Exception {
        T obj = genericType.newInstance();

        if (obj instanceof DeserializePackage) {
            ((DeserializePackage) obj).onDeserialize(this);
            return obj;
        }

        Class instanceClass = obj.getClass();

        int transferFieldsCount = 0;
        Field[] declaredFields = instanceClass.getDeclaredFields();
        Field[] transferFields = new Field[declaredFields.length];

        for (Field field: declaredFields) {
            TransferField transferFieldAnnotation = field.getAnnotation(TransferField.class);

            if (transferFieldAnnotation == null) {
                continue;
            }

            if (!field.isAccessible()) {
                field.setAccessible(true);
            }

            transferFieldsCount++;
            transferFields[transferFieldAnnotation.order()] = field;
        }

        for (int i = 0; i < transferFieldsCount; ++i) {
            Field field = transferFields[i];

            TransferField transferFieldAnnotation = field.getAnnotation(TransferField.class);

            switch (transferFieldAnnotation.type()) {
                case Bool:
                    field.setBoolean(obj, get() == 1);
                    break;

                case Byte:
                    field.setByte(obj, get());
                    break;

                case Short:
                    field.setShort(obj, getShort());
                    break;

                case Int:
                    field.setInt(obj, getInt());
                    break;

                case Long:
                    field.setLong(obj, getLong());
                    break;

                case Float:
                    field.setFloat(obj, getFloat());
                    break;

                case Double:
                    field.setDouble(obj, getDouble());
                    break;

                case String:
                    field.set(obj, getString());
                    break;

                case ChunkString:
                    field.set(obj, getChunkString());
                    break;

                case List:
                    Class<?> listGenericType = transferFieldAnnotation.listGenericType();
                    field.set(obj, getTransferList(listGenericType));
                    break;
            }
        }

        return obj;
    }

    public BufferStream putTransferClass(Object obj) throws Exception {
        if (obj instanceof SerializablePackage) {
            ((SerializablePackage) obj).onSerialize(this);
            return this;
        }

        Class instanceClass = obj.getClass();

        int transferFieldsCount = 0;
        Field[] declaredFields = instanceClass.getDeclaredFields();
        Field[] transferFields = new Field[declaredFields.length];

        for (Field field: declaredFields) {
            TransferField transferFieldAnnotation = field.getAnnotation(TransferField.class);

            if (transferFieldAnnotation == null) {
                continue;
            }

            if (!field.isAccessible()) {
                field.setAccessible(true);
            }

            transferFieldsCount++;
            transferFields[transferFieldAnnotation.order()] = field;
        }

        for (int i = 0; i < transferFieldsCount; ++i) {
            Field field = transferFields[i];

            TransferField transferFieldAnnotation = field.getAnnotation(TransferField.class);
            Object value = field.get(obj);

            switch (transferFieldAnnotation.type()) {
                case Bool:
                    put((byte) ((boolean) value ? 1 : 0));
                    break;

                case Byte:
                    put((byte) value);
                    break;

                case Short:
                    putShort((short) value);
                    break;

                case Int:
                    putInt((int) value);
                    break;

                case Long:
                    putLong((long) value);
                    break;

                case Float:
                    putFloat((float) value);
                    break;

                case Double:
                    putDouble((double) value);
                    break;

                case String:
                    putString((String) value);
                    break;

                case ChunkString:
                    putChunkString((String) value);
                    break;

                case List:
                    putTransferList((AbstractList) value);
                    break;
            }
        }

        return this;
    }

    public BufferStream getFile(File file) throws IOException {
        if (!file.exists()) {
            file.createNewFile();
        }

        int size = mBuffer.getInt();
        return getOutputStream(new FileOutputStream(file), size);
    }

    public BufferStream putFile(File file) throws IOException {
        mBuffer.putInt((int) file.length());
        return putInputStream(new FileInputStream(file));
    }

    public BufferStream putInputStream(FileInputStream in) throws IOException {
        FileChannel inChannel = in.getChannel();

        while (inChannel.read(mBuffer) != -1);
        inChannel.close();

        return this;
    }

    public BufferStream getOutputStream(FileOutputStream out, int size) throws IOException {
        FileChannel outChannel = out.getChannel();

        if (size > mBuffer.remaining()) {
            throw new BufferUnderflowException();
        }

        int limit = mBuffer.limit();

        mBuffer.limit(mBuffer.position() + size);
        outChannel.write(mBuffer);
        mBuffer.limit(limit);

        outChannel.close();

        return this;
    }

    public int limit() {
        return mBuffer.limit();
    }

    public int position() {
        return mBuffer.position();
    }

    public int capacity() {
        return mBuffer.capacity();
    }

    public int remaining() {
        return mBuffer.remaining();
    }

    public void clear() {
        mBuffer.clear();
    }

    public ByteBuffer getBuffer() {
        return mBuffer;
    }

    public BufferStream flip() {
        mBuffer.flip();
        return this;
    }

    public void free() {
        BufferUtils.free(mBuffer);
    }

}
