package com.remspy.access.point.net;

import com.remspy.access.point.net.utils.BufferUtils;

import java.nio.ByteBuffer;

public class PackageWriter {

    protected ByteBuffer mBuffer;

    protected int mHeaderSize, mBufferSize, mPackageId;

    protected int mPositionOfPackageId, mPositionOfSize;

    protected boolean mHavePackageId;

    public PackageWriter(int size) {
        this(size, true);
    }

    public PackageWriter(int size, boolean havePackageId) {
        mHavePackageId = havePackageId;
        initBuffer(size);
    }

    public void setPackageId(int packageId) {
        mPackageId = packageId;
    }

    protected ByteBuffer onBufferInit() {
        return BufferUtils.allocate(mHeaderSize + mBufferSize);
    }

    protected void initBuffer(int size) {
        mBufferSize = size;
        mHeaderSize += 4;
        if (mHavePackageId) {
            mHeaderSize += 4;
        }

        mBuffer = onBufferInit();

        if (mHavePackageId) {
            mPositionOfPackageId = mBuffer.position();
            mBuffer.putInt(0);
        }

        mPositionOfSize = mBuffer.position();
        mBuffer.putInt(0);
    }

    public ByteBuffer getBuffer() {
        return mBuffer;
    }

    public BufferStream getBufferStream() {
        return new BufferStream(mBuffer);
    }

    public ByteBuffer build() {
        int size = mBuffer.position() - mHeaderSize;
        int p = mBuffer.position();

        if (mHavePackageId) {
            mBuffer.position(mPositionOfPackageId);
            mBuffer.putInt(mPackageId);
        }

        mBuffer.position(mPositionOfSize);
        mBuffer.putInt(size);
        mBuffer.position(p);
        ByteBuffer buffer = mBuffer.duplicate();
        buffer.flip();
        return buffer;
    }

    public int available() {
        return mBuffer.position() - mHeaderSize;
    }

    public void free() {
        BufferUtils.free(mBuffer);
    }

    public void reset() {
        mBuffer.clear();
        mBuffer.position(mHeaderSize);
    }

}
