package com.remspy.access.point.net;

import com.remspy.access.point.net.utils.BufferUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class SocketConnector {

    private String mIp;
    private int mPort;
    private SocketChannel mSocketChannel;
    private InetSocketAddress mServerAddr;
    private ByteBuffer mBuffer = BufferUtils.allocate(1024);

    public SocketConnector(String ip, int port) {
        this.mIp = ip;
        this.mPort = port;
        this.mServerAddr = new InetSocketAddress(mIp, mPort);
    }

    public boolean connect() {
        try {
            mSocketChannel = SocketChannel.open();
            boolean connected = mSocketChannel.connect(mServerAddr);
            mSocketChannel.configureBlocking(false);

            return connected;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public void close() {
        try {
            mSocketChannel.close();
        } catch (IOException ignored) {

        }
    }

    public boolean isConnected() {
        return mSocketChannel.isConnected();
    }

    public boolean write(ByteBuffer buffer) {
        try {
            while (buffer.hasRemaining()) {
                mSocketChannel.write(buffer);
            }

            return true;
        } catch (IOException ignored) {
            close();
            return false;
        }
    }

    public ByteBuffer read() {
        try {
            if (mSocketChannel.read(mBuffer) > 0) {
                return mBuffer.duplicate();
            }

            mBuffer.clear();
        } catch (IOException ignored) {
            close();
        }

        return null;
    }

}