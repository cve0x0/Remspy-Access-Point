package com.remspy.access.point;

import com.remspy.access.point.services.AppService;

public class Boot {

    public static void main(String[] args) {
        new AppService().run();
    }

}
