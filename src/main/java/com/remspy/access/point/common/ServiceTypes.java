package com.remspy.access.point.common;

public enum ServiceTypes {
    LISTENER,
    CONSUMER
}