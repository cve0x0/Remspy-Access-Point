package com.remspy.access.point.common;

import com.remspy.access.point.constants.ProtocolConstants;
import com.remspy.access.point.net.PackageReader;
import com.remspy.access.point.net.utils.BufferUtils;

import java.nio.ByteBuffer;

public class PackageBufferReader extends PackageReader {

    public PackageBufferReader() {
        super();
    }

    public PackageBufferReader(boolean havePackageId) {
        super(havePackageId);
    }

    @Override
    protected ByteBuffer initHeaderBuffer(int size) {
        return BufferUtils.allocate(++size);
    }

    @Override
    protected boolean onParseHeader() {
        return mPackageHeader.get() == ProtocolConstants.BUFFER_START;
    }

}
