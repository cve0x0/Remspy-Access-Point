package com.remspy.access.point.common;

import com.remspy.access.point.constants.ProtocolConstants;
import com.remspy.access.point.net.PackageWriter;
import com.remspy.access.point.net.utils.BufferUtils;

import java.nio.ByteBuffer;

public class PackageBufferWriter extends PackageWriter {

    public PackageBufferWriter(int size) {
        super(size);
    }

    public PackageBufferWriter(int size, boolean havePackageId) {
        super(size, havePackageId);
    }

    @Override
    protected ByteBuffer onBufferInit() {
        mHeaderSize += 1;

        ByteBuffer buffer = BufferUtils.allocate(mHeaderSize + mBufferSize);
        buffer.put(ProtocolConstants.BUFFER_START);

        return buffer;
    }

}
