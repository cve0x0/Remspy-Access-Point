package com.remspy.access.point.common;

import com.remspy.access.point.net.BufferStream;

public class ClientPackage {

    private int mPackageId;

    private RequestSession mRequestSession;

    private BufferStream mBufferStream;

    public ClientPackage(RequestSession requestSession, int packageId, BufferStream bufferStream) {
        mRequestSession = requestSession;
        mPackageId = packageId;
        mBufferStream = bufferStream;
    }

    public RequestSession getRequestSession() {
        return mRequestSession;
    }

    public int getPackageId() {
        return mPackageId;
    }

    public BufferStream getBufferStream() {
        return mBufferStream;
    }

}
