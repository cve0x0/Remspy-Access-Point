package com.remspy.access.point.common;

import com.remspy.access.point.annotations.Inject;
import com.remspy.access.point.annotations.Injectable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DependencyInjection {

    public static void inject(ArrayList<Object> modules) {
        Map<Class, Object> injectable = new HashMap<>();

        for (Object m: modules) {
            if (!m.getClass().isAnnotationPresent(Injectable.class)) {
                continue;
            }

            injectable.put(m.getClass(), m);
        }

        for (Object m: modules) {
            ArrayList<Field> declaredFields = new ArrayList<>();

            Class cl = m.getClass();
            do {
                Collections.addAll(declaredFields, cl.getDeclaredFields());
                cl = cl.getSuperclass();
            } while (cl != null);

            for (Field field: declaredFields) {
                if (!field.isAnnotationPresent(Inject.class)) {
                    continue;
                }

                Object inject = injectable.get(field.getType());
                if (inject == null) {
                    continue;
                }

                try {
                    if (!field.isAccessible()) {
                        field.setAccessible(true);
                    }

                    field.set(m, inject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
