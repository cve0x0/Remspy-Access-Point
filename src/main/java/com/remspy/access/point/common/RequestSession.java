package com.remspy.access.point.common;

import java.nio.channels.SocketChannel;

public class RequestSession {

    private int mUUID;

    private boolean mIsAuthenticated;

    private SocketChannel mSocketChannel;

    private PackageBufferReader mPackageReader = new PackageBufferReader();

    public RequestSession(int UUID, SocketChannel socketChannel) {
        mUUID = UUID;
        mSocketChannel = socketChannel;
    }

    public int getUUID() {
        return mUUID;
    }

    public boolean isAuthenticated() {
        return mIsAuthenticated;
    }

    public void setIsAuthenticated(boolean isAuthenticated) {
        mIsAuthenticated = isAuthenticated;
    }

    public SocketChannel getSocketChannel() {
        return mSocketChannel;
    }

    public PackageBufferReader getPackageReader() {
        return mPackageReader;
    }

    public void destroy() {
        mPackageReader.free();
    }

}
