package com.remspy.access.point.utils;

import com.datastax.driver.core.UDTValue;
import com.datastax.driver.core.UserType;
import com.remspy.access.point.modules.providers.contacts.models.ContactGroupModel;

import java.util.ArrayList;
import java.util.Collection;

public class ArrayListUtils {

    public static ArrayList<UDTValue> toUDT(Collection<ContactGroupModel> contactGroupModels, UserType userType) {
        ArrayList<UDTValue> udtValues = new ArrayList<>(contactGroupModels.size());

        for (ContactGroupModel contactGroupModel: contactGroupModels) {
            udtValues.add(
                    userType
                    .newValue()
                    .setString("data", contactGroupModel.getData())
                    .setString("type", contactGroupModel.getType())
            );
        }

        return udtValues;
    }

}
